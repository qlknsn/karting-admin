import { createApp } from 'vue'
import App from './App.vue'
import router from "./router";
import store from "./store";
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
// element加载中文包
import 'dayjs/locale/zh-cn' //中文
import locale from 'element-plus/lib/locale/lang/zh-cn' //中文
import print from 'vue3-print-nb'


const app = createApp(App)
app.use(router)
app.use(store)
app.use(print)
app.use(ElementPlus,{locale})
app.mount('#app')

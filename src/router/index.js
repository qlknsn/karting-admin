// 引入
import { createRouter, createWebHashHistory } from "vue-router";

// 创建
const router = createRouter({
    history: createWebHashHistory(),
    routes: [
        {
            path: '/',
            name: '首页',
            meta: {
                depth: 1
            },
            component: () => import('@/components/home.vue'),
            children: [
                {
                    path: '/',
                    name: '系统首页',
                    meta: {
                        depth: '1',
                        keepAlive: false
                    },
                    component: () => import('@/components/backend/systemFirstpage.vue'),
                },
                {
                    path: '/clubManage',
                    name: '俱乐部管理',
                    meta: {
                        depth: '2',
                        keepAlive: false
                    },
                    component: () => import('@/components/backend/clubManage.vue'),
                },
                {
                    path: '/userManage',
                    name: '用戶管理',
                    meta: {
                        depth: '3',
                        keepAlive: false
                    },
                    component: () => import('@/components/backend/userManage.vue'),
                },
                {
                    path: '/carManage',
                    name: '车辆管理',
                    meta: {
                        depth: '4-1',
                        keepAlive: false
                    },
                    component: () => import('@/components/backend/carManage.vue'),
                },
                {
                    path: '/currentMatch',
                    name: '当前场次',
                    meta: {
                        depth: '5-1',
                        keepAlive: true
                    },
                    component: () => import('@/components/backend/currentMatch.vue'),
                },
                {
                    path: '/historyMatch',
                    name: '历史场次',
                    meta: {
                        depth: '5-2',
                        keepAlive: false
                    },
                    component: () => import('@/components/backend/historyMatch.vue'),
                },
                {
                    path: '/matchModule',
                    name: '比赛模板管理',
                    meta: {
                        depth: '6-1',
                        keepAlive: false 
                    },
                    component: () => import('@/components/backend/matchModule.vue'),
                },
                {
                    path: '/carSearch',
                    name: '车辆查询',
                    meta: {
                        depth: '7-1',
                        keepAlive: false
                    },
                    component: () => import('@/components/backend/carSearch.vue'),
                },
                {
                    path: '/playerSearch',
                    name: '选手查询',
                    meta: {
                        depth: '7-2',
                        keepAlive: false
                    },
                    component: () => import('@/components/backend/playerSearch.vue'),
                },
                {
                    path: '/articleManage',
                    name: '文章管理',
                    meta: {
                        depth: '8',
                        keepAlive: false
                    },
                    component: () => import('@/components/backend/articleManage.vue'),
                },
                {
                    path: '/clubSetting',
                    name: '俱乐部设置',
                    meta: {
                        depth: '9-1',
                        keepAlive: false
                    },
                    component: () => import('@/components/backend/clubSetting.vue'),
                },
                {
                    path: '/systemSetting',
                    name: '系统设置',
                    meta: {
                        depth: '9-2',
                        keepAlive: false
                    },
                    component: () => import('@/components/backend/systemSetting.vue'),
                },
                {
                    path: '/test/carlist',
                    name: '车辆列表',
                    meta: {
                        depth: '9-3',
                        keepAlive: false
                    },
                    component: () => import('@/components/test/carlist.vue'),
                },
            ]
        },
        {
            path: '/login',
            name: '登录页面',
            meta: {
                depth: 2,
                keepAlive: false
            },
            component: () => import('@/components/login.vue')
        },
        {
            path: '/castScreen',
            name: '投屏页面',
            meta: {
                depth: 2,
                keepAlive: false
            },
            component: () => import('@/components/castScreen/castScreen.vue')
        },
    ]
})
export default router;
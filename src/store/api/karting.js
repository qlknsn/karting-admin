import api from "./kartingUrl";
import { axios } from "@/utils/request";
// 登录
export function login(params) {
  return axios({
    url: api.login,
    method: 'post',
    data: params
  });
}
// 获取验证码
export function code(params) {
  return axios({
    url: api.code,
    method: 'get',
    params: params
  });
}
// 获取用户拥有的菜单
export function getUserMenus(params) {
  return axios({
    url: api.getUserMenus,
    method: 'get',
    params: params
  });
}
// 获取俱乐部列表
export function clubList(params) {
  return axios({
    url: api.clubList,
    method: 'post',
    data: params
  });
}
// 图片上传
export function upload(params) {
  return axios({
    url: api.upload,
    method: 'post',
    data: params
  });
}
// 添加俱乐部
export function clubSave(params) {
  return axios({
    url: api.clubSave,
    method: 'post',
    data: params
  });
}
// 车辆列表 
export function clubCarList(params) {
  return axios({
    url: api.clubCarList,
    method: 'post',
    data: params
  });
}
// 添加车辆 
export function clubCarSave(params) {
  return axios({
    url: api.clubCarSave,
    method: 'post',
    data: params
  });
}
// 模板列表 
export function gameTemplateList(params) {
  return axios({
    url: api.gameTemplateList,
    method: 'post',
    data: params
  });
}
// 添加模板列表 
export function gameTemplateSave(params) {
  return axios({
    url: api.gameTemplateSave,
    method: 'post',
    data: params
  });
}
// 获取车手列表 
export function getGameScheduleUser(params) {
  return axios({
    url: api.getGameScheduleUser,
    // method:'post',
    params: params
  });
}
// 新增场次 
export function gameScheduleSave(params) {
  return axios({
    url: api.gameScheduleSave,
    method: 'post',
    data: params
  });
}
// 新增车手
export function addgGameScheduleUser(params) {
  return axios({
    url: api.addgGameScheduleUser,
    method: 'post',
    data: params
  });
}
// 车辆列表
export function availableCars(params) {
  return axios({
    url: api.availableCars,
    params: params
  });
}
// 空余车辆列表
export function getCarExclude(params) {
  return axios({
    url: api.getCarExclude,
    params: params
  });
}
// 转移车手
export function assignGameScheduleUser(params) {
  return axios({
    url: api.assignGameScheduleUser,
    method: 'post',
    data: params
  });
}
// 开始比赛
export function changeGameSchedule(params) {
  return axios({
    url: api.changeGameSchedule,
    method: 'post',
    data: params
  });
}
// 大屏比赛列表
export function gameRank(params) {
  return axios({
    url: api.gameRank,
    method: 'get',
    params: params
  });
}
// 获取当前是否有比赛或者即将进行的比赛
export function getGameScheduleIng(params) {
  return axios({
    url: api.getGameScheduleIng,
    method: 'get',
    params: params
  });
}
// 实时数据
export function realData(params) {
  return axios({
    url: api.realData,
    method: 'get',
    params: params
  });
}
// 添加文章
export function saveArticle(params) {
  return axios({
    url: api.saveArticle,
    method: 'post',
    data: params
  });
}
// 添加文章
export function articleDetail(params) {
  return axios({
    url: api.articleDetail,
    params: params
  });
}
// \文章分类
export function articleCategory(params) {
  return axios({
    url: api.articleCategory,
    params: params
  });
}
// \文章列表
export function articleList(params) {
  return axios({
    url: api.articleList,
    method: "post",
    data: params
  });
}
// 文章删除
export function articleDelete(params) {
  return axios({
    url: `${api.articleDelete}?id=${params.id}`,
    method: "post",
  });
}
// 文章修改
export function articleUpdate(params) {
  return axios({
    url: api.articleUpdate,
    method: "post",
    data: params
  });
}
// 俱乐部详情
export function clubDetail(params) {
  return axios({
    url: api.clubDetail,
    method: "get",
    params: params
  });
}
// 俱乐部修改
export function clubEdit(params) {
  return axios({
    url: api.clubEdit,
    method: "post",
    data: params
  });
}
// 俱乐部删除
export function clubDelete(params) {
  return axios({
    url: api.clubDelete,
    method: "post",
    data: params
  });
}
// 排名
export function rankByPeriod(params) {
  return axios({
    url: api.rankByPeriod,
    params: params
  });
}
// 历史场次
export function gameScheduleList(params) {
  return axios({
    url: api.gameScheduleList,
    method: "post",
    data: params
  });
}
// 删除历史场次
export function gameScheduleDelete(params) {
  return axios({
    url: api.gameScheduleDelete,
    method: "post",
    data: params
  });
}
// 换车
export function changeCar(params) {
  return axios({
    url: `${api.changeCar}?u1=${params.u1}&u2=${params.u2}`,
    method: "post",
  });
}
// 车手列表
export function clubUserList(params) {
  return axios({
    url: api.clubUserList,
    method: "post",
    data: params
  });
}
// 删除车手
export function clubUserDelete(params) {
  return axios({
    url: api.clubUserDelete,
    method: "post",
    data: params
  });
}
// 启用车手
export function clubUserEnable(params) {
  return axios({
    url: api.clubUserEnable,
    method: "post",
    data: params
  });
}
// 禁用车手
export function clubUserDisabled(params) {
  return axios({
    url: api.clubUserDisabled,
    method: "post",
    data: params
  });
}
// 菜单树
export function menuTree() {
  return axios({
    url: api.menuTree,
    method: "get"
  });
}
// 用户详情
export function clubUserDetail(params) {
  return axios({
    url: `${api.clubUserDetail}?id=${params.id}`,
    method: "get",
  });
}
// 用户编辑
export function clubUserUpdate(params) {
  return axios({
    url: api.clubUserUpdate,
    method: "post",
    data: params
  });
}
// 二维码列表
export function qrcodes(params) {
  return axios({
    url: api.qrcodes,
    method: "post",
    data: params
  });
}
// 设置车号
export function updateCarNo(params) {
  return axios({
    url: ` ${api.updateCarNo}?id=${params.id}&carNo=${params.carNo}`,
    method: "post",
  });
}
// 打印历史成绩
export function printUserHistory(params) {
  return axios({
    url: api.printUserHistory,
    method: "post",
    data: params
  });
}
// 打印单场成绩
export function getGameScheduleScore(params) {
  return axios({
    url: api.getGameScheduleScore,
    method: "get",
    params: params
  });
}
// 删除车辆
export function deleteCar(params) {
  return axios({
    url: api.deleteCar,
    method: "post",
    data: params
  });
}
// 编辑比赛模板
export function gameTemplateUpdate(params) {
  return axios({
    url: api.gameTemplateUpdate,
    method: "post",
    data: params
  });
}
// 删除比赛模板
export function gameTemplateDelete(params) {
  return axios({
    url: api.gameTemplateDelete,
    method: "post",
    data: params
  });
}
// 编辑车辆
export function editClubCar(params) {
  return axios({
    url: api.editClubCar,
    method: "post",
    data: params
  });
}
// 车辆禁用
export function disabledClubCar(params) {
  return axios({
    url: api.disabledClubCar,
    method: "post",
    data: params
  });
}
// 车辆启用
export function enabledisabledClubCar(params) {
  return axios({
    url: api.enabledisabledClubCar,
    method: "post",
    data: params
  });
}
// 模板禁用
export function templateDisabled(params) {
  return axios({
    url: api.templateDisabled,
    method: "post",
    data: params
  });
}
// 模板启用
export function templateEnable(params) {
  return axios({
    url: api.templateEnable,
    method: "post",
    data: params
  });
}
// 划分圈速
export function deviceData(params) {
  return axios({
    url: api.deviceData,
    method: "post",
    data: params
  });
}

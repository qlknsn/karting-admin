const api = {
    // 登录
    login:'/karting/sysUser/login',
    // 验证码
    code:'/karting/sysUser/code',
    // 账号拥有的菜单
    getUserMenus:'/karting/menu/getUserMenus',
    // 俱乐部列表
    clubList:'/karting/club/list',
    // 上传图片
    upload:'/karting/club/upload',
    // 添加俱乐部
    clubSave:'/karting/club/save',
    // 车辆列表
    clubCarList:'/karting/clubCar/list',
    // 添加车
    clubCarSave:'/karting/clubCar/save',
    // 比赛模板列表
    gameTemplateList:'/karting/gameTemplate/list',
    // 添加比赛模板列表
    gameTemplateSave:'/karting/gameTemplate/save',
    // 删除比赛模板列表
    gameTemplateDelete:'/karting/gameTemplate/delete',
    // 编辑比赛模板列表
    gameTemplateUpdate:'/karting/gameTemplate/update',
    // 获取车手列表
    getGameScheduleUser:'/karting/gameSchedule/getGameScheduleUser',
    // 新增场次
    gameScheduleSave:'/karting/gameSchedule/save',
    // 新增车手
    addgGameScheduleUser:'/karting/gameSchedule/addgGameScheduleUser',
    // 车辆列表
    availableCars:'/karting/clubCar/availableCars',
    // 空余车辆列表
    getCarExclude:'/karting/clubCar/getCarExclude',
    // 转移车手
    assignGameScheduleUser:' /karting/gameSchedule/assignGameScheduleUser',
    // 开始比赛
    changeGameSchedule:'/karting/gameSchedule/changeGameSchedule',
    //  大屏比赛列表
    gameRank:'/karting/game/rank',
    //  获取当前是否有比赛或者即将进行的比赛
    getGameScheduleIng:'/karting/gameSchedule/getGameScheduleIng',
    //  实时数据
    realData:'/karting/game/realData',
    //  实时数据
    saveArticle:'/karting/article/save',
    //  文章详情
    articleDetail:'/karting/article/detail',
    //  文章分类
    articleCategory:'/karting/article/category',
    //  文章列表
    articleList:'/karting/article/list',
    //  文章删除
    articleDelete:'/karting/article/delete',
    //  文章修改
    articleUpdate:'/karting/article/update',
    //  俱乐部详情
    clubDetail:'/karting/club/detail',
    //  设置俱乐部
    clubEdit:'/karting/club/edit',
    //  删除俱乐部
    clubDelete:'/karting/club/delete',
    //  排名
    rankByPeriod:'/karting/game/rankByPeriod',
    //  历史场次
    gameScheduleList:'/karting/gameSchedule/list',
    //  删除历史场次
    gameScheduleDelete:'/karting/gameSchedule/delete',
    //  换车
    changeCar:'/karting/gameSchedule/changeCar',
    //  车手列表
    clubUserList:'/karting/clubUser/list',
    //  删除车手
    clubUserDelete:'/karting/clubUser/delete',
    //  启用
    clubUserEnable:'/karting/clubUser/enable',
    //  禁用
    clubUserDisabled:'/karting/clubUser/disabled',
    //  菜单树
    menuTree:'/karting/menu/tree',
    //  用户详情
    clubUserDetail:'/karting/clubUser/detail',
    //  用户编辑
    clubUserUpdate:'/karting/clubUser/update',
    //  二维码列表
    qrcodes:'/karting/club/qrcodes',
    //  设置车号
    updateCarNo:'/karting/gameSchedule/updateCarNo',
    //  历史成绩
    printUserHistory:'/karting/game/printUserHistory',
    //  单场成绩
    getGameScheduleScore:'/karting/game/getGameScheduleScore',
    // 删除车辆
    deleteCar:'/karting/clubCar/delete',
    // 编辑车辆
    editClubCar:'/karting/clubCar/edit',
    // 车辆禁用
    disabledClubCar:'/karting/clubCar/disabled',
    // 车辆启用
    enabledisabledClubCar:'/karting/clubCar/enable',
    // 模板禁用
    templateDisabled:'/karting/gameTemplate/disabled',
    // 模板启用
    templateEnable:'/karting/gameTemplate/enable',
    // 划分圈速
    deviceData:'/karting/gameRecord/deviceData',
}
export default api;
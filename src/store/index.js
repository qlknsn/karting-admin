import { createStore } from "vuex";
import karting from "@/store/modules/karting.js";
const store = createStore({
    state(){
        return{

        }
    },
    mutations:{

    },
    actions:{

    },
    modules:{
        karting
    }
})
export default store;
import {
    login,
    code,
    getUserMenus,
    clubList,
    clubSave,
    clubCarList,
    clubCarSave,
    gameTemplateList,
    gameTemplateSave,
    getGameScheduleUser,
    gameScheduleSave,
    addgGameScheduleUser,
    availableCars,
    assignGameScheduleUser,
    changeGameSchedule,
    gameRank,
    getGameScheduleIng,
    getCarExclude,
    realData,
    saveArticle,
    articleDetail,
    articleCategory,
    articleList,
    articleDelete,
    articleUpdate,
    clubDetail,
    clubEdit,
    clubDelete,
    rankByPeriod,
    gameScheduleList,
    gameScheduleDelete,
    changeCar,
    clubUserList,
    clubUserDelete,
    clubUserEnable,
    clubUserDisabled,
    menuTree,
    clubUserDetail,
    clubUserUpdate,
    qrcodes,
    updateCarNo,
    printUserHistory,
    getGameScheduleScore,
    deleteCar,
    editClubCar,
    disabledClubCar,
    enabledisabledClubCar,
    gameTemplateUpdate,
    gameTemplateDelete,
    templateDisabled,
    templateEnable,
    deviceData
} from "@/store/api/karting";
const state = {
    name: 's',
    gameRankList:[{gameType:'JS'}],
    prepareUsers:[],
    wswaitUsers:[],
    realdata:[]
}

const actions = {
    login({ commit }, params) {
        let p = login(params)
        return p;
    },
    code({ commit }, params) {
        let p = code(params)
        return p;
    },
    getUserMenus({ commit }, params) {
        let p = getUserMenus(params)
        return p;
    },
    clubList({ commit }, params) {
        let p = clubList(params)
        return p;
    },
    clubSave({ commit }, params) {
        let p = clubSave(params)
        return p;
    },
    clubCarList({ commit }, params) {
        let p = clubCarList(params)
        return p;
    },
    clubCarSave({ commit }, params) {
        let p = clubCarSave(params)
        return p;
    },
    gameTemplateList({ commit }, params) {
        let p = gameTemplateList(params)
        return p;
    },
    gameTemplateSave({ commit }, params) {
        let p = gameTemplateSave(params)
        return p;
    },
    gameTemplateDelete({ commit }, params) {
        let p = gameTemplateDelete(params)
        return p;
    },
    getGameScheduleUserbig({ commit }, params) {
        let p = getGameScheduleUser(params)
        p.then(res=>{
            commit('GET_PREPARE_USERS',res.data)
        })
    },
    getGameScheduleUserwait({ commit }, params) {
        let p = getGameScheduleUser(params)
        p.then(res=>{
            commit('GET_PREPARE_USERS_WAIT',res.data)
        })
    },
    getGameScheduleUser({ commit }, params) {
        let p = getGameScheduleUser(params)
       return p;
    },
    gameScheduleSave({ commit }, params) {
        let p = gameScheduleSave(params)
        return p;
    },
    addgGameScheduleUser({ commit }, params) {
        let p = addgGameScheduleUser(params)
        return p;
    },
    availableCars({ commit }, params) {
        let p = availableCars(params)
        return p;
    },
    getCarExclude({ commit }, params) {
        let p = getCarExclude(params)
        return p;
    },
    gameTemplateUpdate({ commit }, params) {
        let p = gameTemplateUpdate(params)
        return p;
    },
    assignGameScheduleUser({ commit }, params) {
        let p = assignGameScheduleUser(params)
        return p;
    },
    changeGameSchedule({ commit }, params) {
        let p = changeGameSchedule(params)
        return p;
    },
    realData({ commit }, params) {
        let p = realData(params)
        return p;
    },
    getGameScheduleIng({ commit }, params) {
        let p = getGameScheduleIng(params)
        return p;
    },
    saveArticle({ commit }, params) {
        let p = saveArticle(params)
        return p;
    },
    articleDetail({ commit }, params) {
        let p = articleDetail(params)
        return p;
    },
    articleCategory({ commit }, params) {
        let p = articleCategory(params)
        return p;
    },
    articleList({ commit }, params) {
        let p = articleList(params)
        return p;
    },
    articleDelete({ commit }, params) {
        let p = articleDelete(params)
        return p;
    },
    articleUpdate({ commit }, params) {
        let p = articleUpdate(params)
        return p;
    },
    clubDetail({ commit }, params) {
        let p = clubDetail(params)
        return p;
    },
    clubEdit({ commit }, params) {
        let p = clubEdit(params)
        return p;
    },
    clubDelete({ commit }, params) {
        let p = clubDelete(params)
        return p;
    },
    rankByPeriod({ commit }, params) {
        let p = rankByPeriod(params)
        return p;
    },
    gameScheduleList({ commit }, params) {
        let p = gameScheduleList(params)
        return p;
    },
    gameScheduleDelete({ commit }, params) {
        let p = gameScheduleDelete(params)
        return p;
    },
    changeCar({ commit }, params) {
        let p = changeCar(params)
        return p;
    },
    clubUserList({ commit }, params) {
        let p = clubUserList(params)
        return p;
    },
    clubUserDelete({ commit }, params) {
        let p = clubUserDelete(params)
        return p;
    },
    clubUserEnable({ commit }, params) {
        let p = clubUserEnable(params)
        return p;
    },
    clubUserDisabled({ commit }, params) {
        let p = clubUserDisabled(params)
        return p;
    },
    menuTree({ commit }, params) {
        let p = menuTree(params)
        return p;
    },
    clubUserDetail({ commit }, params) {
        let p = clubUserDetail(params)
        return p;
    },
    clubUserUpdate({ commit }, params) {
        let p = clubUserUpdate(params)
        return p;
    },
    qrcodes({ commit }, params) {
        let p = qrcodes(params)
        return p;
    },
    updateCarNo({ commit }, params) {
        let p = updateCarNo(params)
        return p;
    },
    printUserHistory({ commit }, params) {
        let p = printUserHistory(params)
        return p;
    },
    editClubCar({ commit }, params) {
        let p = editClubCar(params)
        return p;
    },
    disabledClubCar({ commit }, params) {
        let p = disabledClubCar(params)
        return p;
    },
    // 禁用比赛模板
    templateDisabled({ commit }, params) {
        let p = templateDisabled(params)
        return p;
    },
    // 启用比赛模板
    templateEnable({ commit }, params) {
        let p = templateEnable(params)
        return p;
    },
    // 划分圈速
    deviceData({ commit }, params) {
        let p = deviceData(params)
        return p;
    },
    enabledisabledClubCar({ commit }, params) {
        let p = enabledisabledClubCar(params)
        return p;
    },
    getGameScheduleScore({ commit }, params) {
        let p = getGameScheduleScore(params)
        return p;
    },
    deleteCar({ commit }, params) {
        let p = deleteCar(params)
        return p;
    },
    gameRank({ commit }, params) {
        let p = gameRank(params)
        p.then(res=>{
            commit('GET_GAMERANK',res.data)
        })
    },
}
const mutations = {
    GET_GAMERANK(state,res){
        state.gameRankList = res
    },
    GET_PREPARE_USERS(state,res){
        state.prepareUsers = res
    },
    GET_PREPARE_USERS_WAIT(state,res){
        console.log(res);
        state.wswaitUsers = res
    },
    GET_REAL_DATA(state,res){
        console.log('====================================');
        console.log(res);
        console.log('====================================');
        state.realdata = res
    },

}
export default {
    state,
    mutations,
    actions
}
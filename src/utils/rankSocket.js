import store from '@/store/index'

let socket = null
const WS_BASE_URL = `ws://47.100.229.123:8083/karting`
// const WS_BASE_URL = `wss://aikarting.net/wskarting`
let timer = null
export function WSconnect(clubId) {

    socket = new WebSocket(WS_BASE_URL)
    socket.onopen = () => {
        let registerJson = {
            clubId:clubId
        }
        socket.send(JSON.stringify(registerJson))
        console.log('比赛开始了');
    }

    socket.onerror = () => {
        // setInterval(() => {
        //      WSconnect()
        // }, 3000);
       
    }
    socket.onmessage = (evt) => {
        let wsdata = JSON.parse(evt.data)
        if(wsdata.data){
            if(wsdata.type=="rank"){
                // 实时数据
                store.commit('GET_GAMERANK',wsdata.data)
            }else if(wsdata.type=="waitUsers"){
                // 等待车手列表
                store.commit('GET_PREPARE_USERS_WAIT',wsdata.data)
            }else if(wsdata.type=="prepareUsers"){
                // 准备车手列表
                store.commit('GET_PREPARE_USERS',wsdata.data)
            }else if(wsdata.type=="realData"){
                // 实时数据
                store.commit('GET_REAL_DATA',wsdata.data)
            }
            
        }
        
    };
    socket.onclose = () => {
        setTimeout(() => {
            WSconnect(clubId)   
        }, 10000);
    };
}
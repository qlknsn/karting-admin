import axios from "axios";
// import { useRouter } from 'vue-router';

// 创建 axios 实例
const service = axios.create({
  baseURL: '',
  timeout: 60000, // 请求超时时间
  onUploadProgress: function (progressEvent) {
    progressEvent;
  }
});

const err = error => {
  console.log(error.response);
  if (error.response) {
    //   const data = error.response.data;

    // todo: 需要对503状态吗进行处理
    if (error.response.status === 401) {
      // const router = useRouter()
      // this.$router.push('/login')
      let url = window.location.origin
      window.location.href =`${url}/#/login`
    }
    if (error.response.status == 404) {
      alert('api未找到');
    }
    if (error.response.status === 400) {
      alert("接口请求出错");
    }
    if (error.response.status === 403) {
      alert("forbidden");
    }
    if (error.response.status === 503) {
      alert(网关错误)
    }
    if (error.response.status === 500) {
      alert('服务器错误')
    }
  }
  return Promise.reject(error);
};

// request interceptor
service.interceptors.request.use(config => {
  const token = localStorage.getItem("token");
  if (token) {
    config.headers["token"] = token
  }
  return config;
}, err);

// response interceptor
service.interceptors.response.use(response => {

  return response.data;
}, err);


export { service as axios };
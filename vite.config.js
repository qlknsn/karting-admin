import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  
  server: {
    hmr: true,
    host: '0.0.0.0',
    cors: true,
    open: true,
    proxy: {
      '/karting': {
        target: 'http://47.100.229.123:8080/karting',   //代理接口
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/karting/, '')
      }
    }
  },
  resolve: {
    alias: {
      '@': path.resolve(__dirname, 'src'),
    }
  },
  plugins: [vue()],
  build: {
    outDir: 'kt', //指定输出路径
    chunkSizeWarningLimit:2000,
		rollupOptions: {
			output: {
				//解决打包时Some chunks are larger警告
				manualChunks(id) {
          if (id.includes('node_modules')) {
            return 'vendor';
          }
        }
			}
		}
	},
  css: {
    // css预处理器
    preprocessorOptions: {
      // less: {
      //   charset: false,
      //   additionalData: '@import "./src/assets/style/global.less";',
      // },
    },
    
  },
})
